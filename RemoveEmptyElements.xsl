<xsl:template match="node()">
    <xsl:if test="count(descendant::text()[string-length(normalize-space(.))>0]|@*)">
      <xsl:copy>
        <xsl:apply-templates select="@*|node()"/>
      </xsl:copy>
    </xsl:if>
</xsl:template>
<xsl:template match="@*">
    <xsl:copy/>
</xsl:template>  
<xsl:template match="text()">    
    <xsl:value-of select="normalize-space(.)"/>        
</xsl:template>
<xsl:template match="node()">    
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
</xsl:template>